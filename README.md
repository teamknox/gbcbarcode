# Barcode reader handling by Gameboy color
Program example for connection with BarCode Reader which has been sold by Cyokyu-Dennoh in Akihabara.

## How it works
Basic mechanism as same as GB-KEY. However, different code will be generated.

## Examples
<table>
<tr>
<td><img src="./pics/gb-bar_fr.jpg"></td>
<td><img src="./pics/gb-bar_rr.jpg"></td>
</tr>
</table>

## Episode
My daughter (around 3ys) and I had tried (started to play) it in convinience store where is located close to my flat, old part-time sales-guy stopped us. Good memory this project :-) 

# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 

