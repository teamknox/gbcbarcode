//
// IBM BarCode Reader
//      TeamKNOx
//      11/2000

// This BarCode Reader gives Header, BarCode ID, and Footer as follows,
// Header:            0x82
// BarCode ID: JAN13  0x41
//             JAN8   0x42
//             ITF    0x49
//             CODE39 0x4D
// Footer:            0x83

#include <gb.h>
#include <stdlib.h>
#include "bkg.h"


// extern
extern UBYTE key_raw_ir[2];

// definition
#define OFF 0
#define ON 1

#define FOREVER 1
#define READING 1

#define CLEAR 0
#define DATA_BUF_SIZE 20

#define HEADER_ID 0x82
#define FOOTER_ID 0x83

#define JAN13 0x41
#define JAN8 0x42
#define ITF 0x49
#define CODE39 0x4D
#define NW7 0x4E

/* character data */
#include "bkg.c"
UWORD bkg_palette[] = {
	bkgCGBPal0c0, bkgCGBPal0c1, bkgCGBPal0c2, bkgCGBPal0c3,
};

/* message */
unsigned char msg_title[]  = { "[IBM BarCode Reader]" };
unsigned char msg_credit[] = { "`2000 TeamKNOx" };
unsigned char msg_clr[]    = { "                   " };
unsigned char msg_frame1[] = { 16,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,17,18,0 };
unsigned char msg_frame2[] = { 19,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,20,0 };
unsigned char msg_frame3[] = { 21,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,22,23,0 };

unsigned char msg_spc[]    = { " " };

unsigned char msg_jan13[]  = { "JAN13  " };
unsigned char msg_jan8[]   = { "JAN8   " };
unsigned char msg_code39[] = { "CODE39 " };
unsigned char msg_nw7[]    = { "NW7    " };
unsigned char msg_itf[]    = { "ITF    " };
unsigned char msg_err[]    = { "ERROR !" };

/* work area */

/* functoins */
void init_character()
{
	set_bkg_data( 0, 128, bkg );
	set_bkg_palette( 0, 1, bkg_palette );
	SHOW_BKG;
	DISPLAY_ON;
	enable_interrupts();
}

void cls( UBYTE y1, UBYTE y2 )
{
	UBYTE y;
	for ( y=y1; y<y2+1; y++ ) {
		set_bkg_tiles( 0, y, 20, 1, msg_clr );
	}
}

void disp_value( UWORD d, UWORD e, UBYTE c, UBYTE x, UBYTE y )
{
	UWORD m;
	UBYTE i, n;
	unsigned char data[6];

	m = 1;
	if ( c > 1 ) {
		for ( i = 1; i < c; i++ ) {
			m = m * e;
		}
	}
	for ( i = 0; i < c; i++ ) {
		n = d / m; d = d % m; m = m / e;
		data[i] = '0' + n;		 /* '0' - '9' */
		if ( data[i] > '9' )  data[i] += 7;
	}
	set_bkg_tiles( x, y, c, 1, data );
}

void main()
{
	UBYTE flag, i;
	unsigned char data[DATA_BUF_SIZE + 1];

	init_character();
	/* initialize bank number */
	ENABLE_RAM_MBC1;
	SWITCH_RAM_MBC1( 0 );
	cls( 0, 18 );
	set_bkg_tiles( 0, 2, 20, 1, msg_title );
	set_bkg_tiles( 3, 16, 14, 1, msg_credit );
	set_bkg_tiles( 0, 7, 20, 1, msg_frame1 );
	set_bkg_tiles( 0, 8, 20, 1, msg_frame2 );
	set_bkg_tiles( 0, 9, 20, 1, msg_frame3 );

	while( FOREVER ) {

		// Clear Buffer value
		for( i = 0; i <= DATA_BUF_SIZE; i++ ) {
			data[i] = CLEAR;
		}

		// Wait BarCode signal
		flag = READING;
		i = 0;
		while( flag ) {
			flag = get_raw_signal_key();
			data[i] = key_raw_ir[0];
			i++;
		}

		// Read out & display data
		// Footer data has been fixed as 0x83.
		// Until encountered footer data.
		if( data[1] != CLEAR ) set_bkg_tiles( 0, 8, 20, 1, msg_frame2 );
		i = 2;
		while( (i < DATA_BUF_SIZE) && (data[i] != CLEAR) && (data[i] != FOOTER_ID) ) {
			set_bkg_tiles( i - 1, 8, 1, 1, &data[i] );
			i++;
		}

		// Display supplement data as Header, BarCode ID, Footer
		if( data[1] ) {	// Data has been already read out ?
			// Header
			disp_value( data[0], 16, 2, 1, 6 );
			
			// BarCode ID
			disp_value( data[1], 16, 2, 4, 6 );
			
			switch ( data[1] ) {
				case JAN13:  set_bkg_tiles( 8, 6, 7, 1, msg_jan13 ); break;
				case JAN8:   set_bkg_tiles( 8, 6, 7, 1, msg_jan8  ); break;
				case CODE39: set_bkg_tiles( 8, 6, 7, 1, msg_code39); break;
				case ITF:    set_bkg_tiles( 8, 6, 7, 1, msg_itf   ); break;
				case NW7:    set_bkg_tiles( 8, 6, 7, 1, msg_nw7   ); break;
				default:     set_bkg_tiles( 8, 6, 7, 1, msg_err   ); break;
			}
			
			// Footer
			disp_value( data[i], 16, 2, 1, 10 );
		}
	}
}

/* EOF */